// Inserting Data
db.fruits.insertMany([
    {
        name: "Apple",
        color: "Red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: ["Philippines", "US"]
    },

    {
        name: "Banana",
        color: "Yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: ["Philippines", "Ecuador"]
    },

    {
        name: "Kiwi",
        color: "Green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: ["US", "China"]
    },

    {
        name: "Mango",
        color: "Yellow",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: ["Philippines", "India"]
    }
]);


// 1. Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
    {
      $match: { onSale : true }
    },
    { $count: "fruitsOnSale"}
]);


// 2.  Use the count operator to count the total number of fruits with stock more than 20.

db.fruits.aggregate([
    {
      $match: { stock : { $gte: 20 } }
    },
    { $count: "enoughStock"}
]);


// 3.  Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate ([
    {
        $match: { onSale: true} //1st phase
    },
    {
        $group: {
            _id: "$supplier_id", //// responsible what do you want to group and _ id is the title of folder
            avg_price: { $avg: "$price"}// 2nd phase this is the operators of price
           
        }
    }
]);


// 4.  Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate ([
    {
        $match: { onSale: true} //1st phase
    },
    {
        $group: {
            _id: "$supplier_id", //// responsible what do you want to group and _ id is the title of folder
            max_price: { $max: "$price"}// 2nd phase this is the operators of  price
           
        }
    }
]);


//5.  Use the min operator to get the lowest price of a fruit per supplier. and sort in ascending order
db.fruits.aggregate ([
    {
        $match: { onSale: true} //1st phase
    },
    {
        $group: {
            _id: "$supplier_id", //// responsible what do you want to group and _ id is the title of folder
            min_price: { $min: "$price"}// 2nd phase this is the operators of price
           
        }
    },
    {
        $sort: { price: 1}
    }
]);