// Aggregation in MongoDB and Query Case Studies.

// Inserting Data
db.fruits.insertMany([
    {
        name: "Apple",
        color: "Red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: ["Philippines", "US"]
    },

    {
        name: "Banana",
        color: "Yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: ["Philippines", "Ecuador"]
    },

    {
        name: "Kiwi",
        color: "Green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: ["US", "China"]
    },

    {
        name: "Mango",
        color: "Yellow",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: ["Philippines", "India"]
    }
]);


// Aggregation Pipeline

/* 
    Documentation on aggregation
    - https://www.mongodb.com/docs/manual/reference/operator/aggregation-pipeline/

    1st Phase - $match phase is responsible for gathering the initial items base on a certain argument/criteria

    2nd Phase - $group phase is responsible for grouping the specific fields from the documents after teh criteria has been determined.

    3rd Phase - Optional - $project phase is responsible for excluding certain fields that do not need to show up in the final result
*/

// 1 Stage only
// .count() - is for counting an object or specific elements or fields.

db.fruits.count();


// the $count stage returns a count of the remaining documents in the aggregation pipeline and assigns the value to a field.
// "fruits" is changeable depends on what do you want to name
db.fruits.aggregate([
    { $count: "fruits"}
]);

//========================================

// 2 Stage only
// $count with $match
db.fruits.aggregate([
    {
      $match: { onSale : true }
    },
    { $count: "fruity"}
]);

// $count with $match with $gte
db.fruits.aggregate([
    {
      $match: { price : { $gte: 50 } }
    },
    { $count: "fruity"}
]);

// $match and $group
db.fruits.aggregate ([
    {
        $match: { onSale: true} //1st phase
    },
    {
        $group: {
            _id: "supplier_id", // responsible what do you want to group and _ id is the title of folder
            totalStocks: { $sum: "$stock"} // 2nd phase this is the operators
        }
    }
]);

// $match and $group
// input $ on supplier_id to group it on the same supplier_id and the stocks will combine
db.fruits.aggregate ([
    {
        $match: { onSale: true} //1st phase
    },
    {
        $group: {
            _id: "$supplier_id", //// responsible what do you want to group and _ id is the title of folder
            totalStocks: { $sum: "$stock"},// 2nd phase this is the operators of stocks and price
            totalPrice: { $sum: "$price"}
        }
    }
]);


// 3rd Phase

db.fruits.aggregate ([
    {
        $match: { onSale: true} //1st phase
    },
    {
        $group: {
            _id: "$supplier_id", // responsible what do you want to group and _ id is the title of folder
            totalStocks: { $sum: "$stock"},// 2nd phase this is the operators of stocks and price
            totalPrice: { $sum: "$price"}
        }
    },
    {
        $project: { totalStocks: 0, _id: 0 } // like inclusion and exclusion can be used true or false also id and total stocks will be excluded
    }
]);

// $sort operator - responsible for sorting/arranging items in the result based on their value. (1 means ascending, -1 means decsending)
db.fruits.aggregate ([
    {
        $match: { onSale: true} //1st phase
    },
    {
        $group: {
            _id: "$supplier_id",  // responsible what do you want to group and _ id is the title of folder
            totalStocks: { $sum: "$stock"},// 2nd phase this is the operators of stocks and price
            totalPrice: { $sum: "$price"}
        }
    },
    {
        $project: { totalStocks: 0, _id: 0 } // like inclusion and exclusion can be used true or false also id and total stocks will be excluded
    },
    {
        $sort: { price: 1 } // this will be from smallest to largest price (ascending)
    }
]);



// $unwind operator - responsible for deconstructing an array and using them as the unique identifiers for each row in the result.
// applicable on array only
// this will segregate the array inside of the object
db.fruits.aggregate([
    { $unwind: "$origin"}
]);

// with $project and $match
db.fruits.aggregate([
    { $unwind: "$origin"},
    { $project: {origin:1 , name: 1, onSale: 1}},
    { $match: { onSale: true}}
]);

// with group
db.fruits.aggregate([
    { $unwind: "$origin" },
    { $group : { _id : "$origin", fruitsAvail : { $sum : 1 } } } // kinds: will show all the kinds of fruits per origin 
]);